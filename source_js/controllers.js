/* Ride results page controller */

app.controller('rideController', ['$scope', '$http', function($scope, $http) {

    // Code for ride results page
    $scope.IsInvisible = false;
    $scope.IsVisible = true;
    $scope.MoreShowHide = function () {
        $scope.IsInvisible = !$scope.IsInvisible;
    }
    $scope.number = 5;
    $scope.getNumber = function(num) {
    return new Array(num);
    }
    // Google Maps autocomplete
    var from = document.getElementById('from');
    var to = document.getElementById('to');
    var autocompleteFrom = new google.maps.places.Autocomplete(from);
    var autocompleteTo = new google.maps.places.Autocomplete(to);

    // searchFilter uses userInput to filter results
    $scope.searchFilter = {};
    $scope.userInput = {};

    $http.get('./data/drivers.json').success(function(data){
        $scope.drivers = data;
        console.log(data);
    }).error(function(err){
        console.log(err);
    })

    $scope.sortOptions = [
        { name: 'Recommended', id: 'recommended' },
        { name: 'Departure Time', id: 'time' },
        { name: 'Price', id: 'price' },
        { name: 'User Rating', id: 'rating' },
        { name: 'Top Music Match', id: 'music' },
        { name: 'Common Interests', id: 'interests' },
    ];
    $scope.sortDirections = [{ name: 'Ascending', id: false }, { name: 'Descending', id: true }];    //reverse?
    $scope.sortOption = $scope.sortOptions[1];
    $scope.sortDirection = $scope.sortDirections[0];

    $scope.passengerNumOptions = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
    $scope.numPassengers = 1;

    $scope.bagNumOptions = [ 0, 1, 2, 3, 4 ];
    $scope.numBags = 0;

    // Extra filters on the side
    $scope.userFilters = {};
    // $scope.applyFilters = function(){
    //     console.log("Apply filters button pressed");
    //     // Look at everything the user has input
    //     for(prop in $scope.userFilters) {
    //         $scope.searchFilter[prop] = $scope.userFilters[prop];
    //         console.log(prop + " = " + $scope.userFilters[prop]);
    //     }
    // }

    $scope.musicOptions = [
        { name: 'Alternative', id: 'alt' },
        { name: 'Blues', id: 'blues' },
        { name: 'Classical', id: 'classical' },
        { name: 'Country', id: 'country' },
        { name: 'Dance', id: 'dance' },
        { name: 'Folk', id: 'folk' },
        { name: 'Hip Hop', id: 'hiphop' },
        { name: 'Indie', id: 'indie' },
        { name: 'Jazz', id: 'jazz' },
        { name: 'Metal', id: 'metal' },
        { name: 'Rap', id: 'rap' },
        { name: 'Reggae', id: 'reggae' },
        { name: 'Rock', id: 'rock' },
    ];

    $scope.selectedMusicOptions = [];

    $scope.carOptions = [
        { name: 'Compact', id: 'compact' },
        { name: 'Convertible', id: 'convertible' },
        { name: 'Crossover', id: 'crossover' },
        { name: 'Hatchback', id: 'hatchback' },
        { name: 'Hybrid/Electric', id: 'hybrid' },
        { name: 'Luxury', id: 'luxury' },
        { name: 'Sedan', id: 'sedan' },
        { name: 'SUV', id: 'suv' },
        { name: 'Truck', id: 'truck' },
        { name: 'Sports Car', id: 'sports' }
        ];

    $scope.selectedCarOptions = [];

    //When a user presses the search button
    $scope.search = function(){
        // Clear the search filters and reassign
        /*
        $scope.searchFilter = {};
        console.log("searchFilter = " + Object.keys($scope.searchFilter));

        console.log("to: " + $scope.userInput["to"] + "\n" + "from: " + $scope.userInput["from"]);
        $scope.searchFilter["to"] = $scope.userInput["to"];
        $scope.searchFilter["from"] = $scope.userInput["from"];

        console.log("searchFilter = " + Object.keys($scope.searchFilter));
        */
    };

    //filter
    //$scope.applyFilters = function () {
    $scope.searchFilter = function (driver) {
        return driver.price >= $scope.minPrice && driver.price <= $scope.maxPrice
        && (driver.time.hour > $scope.userFilters.earliestTime.hour ||
            (driver.time.hour == $scope.userFilters.earliestTime.hour && driver.time.minute >= $scope.userFilters.earliestTime.minute ))
        && (driver.time.hour < $scope.userFilters.latestTime.hour ||
            (driver.time.hour == $scope.userFilters.latestTime.hour && driver.time.minute <= $scope.userFilters.latestTime.minute ));
    }
    //}


    // Price slider
    $scope.minPrice = document.getElementById("sliderLower").value;
    $scope.maxPrice = document.getElementById("sliderUpper").value;
    var elem = new Foundation.Slider($("#priceSlider"), {"data-double-sided": true, "data-binding": true, "data-draggable":true});
     $('#priceSlider').on('moved.zf.slider', function(){
        console.log("Price changed");
        $scope.minPrice = document.getElementById("sliderLower").value;
        $scope.maxPrice = document.getElementById("sliderUpper").value;
        $scope.$apply();
    });

     // Time slider

     $scope.userFilters = {
        "earliestTime": {
            "hour": 0,
            "displayHour": 12,
            "minute": 0,
            "displayMinute": 00,
            "am_pm": "AM"
        },
        "latestTime": {
            "hour": 0,
            "displayHour": 12,
            "minute": 0,
            "displayMinute": 00,
            "am_pm": "AM"
        }
    };

     var elem = new Foundation.Slider($("#timeSlider"), {"data-double-sided": true, "data-binding": true, "data-draggable":true});
     $('#timeSlider').on('moved.zf.slider', function(){
        console.log("Time changed");
        var sliderLower = document.getElementById("timeSliderLower").value;
        var sliderUpper = document.getElementById("timeSliderUpper").value;

        var earliestHour = Math.floor(sliderLower);
        var earliestMin = Math.floor((sliderLower - earliestHour) * 60);

        var latestHour = Math.floor(sliderUpper);
        var latestMin = Math.floor((sliderUpper - latestHour) * 60);

        console.log($scope.userFilters.earliestTime)

        // Set display hour & am/pm correctly
        var setDisplayTime = function(hour, minute, isEarliest){

            var userDisplayHour, userDisplayMinute, userAmPm;

            // Set display hour
            // Midnight is different
            if (hour === 0) {
                userDisplayHour = 12;
                userAmPm = "AM";
            }
            // Noon is also different
            else if (hour === 12){
                userDisplayHour = hour;
                userAmPm = "PM";
            }
            // PM subtract 12
            else if (hour > 12){
                userDisplayHour = hour - 12;
                userAmPm = "PM";
            }
            // AM is normal
            else {
                userDisplayHour = hour;
                userAmPm = "AM";
            }

            // Set display minute
            if (minute < 10){
                userDisplayMinute = "0" + minute.toString();
            }
            else {
                userDisplayMinute = minute.toString();
            }

            // Set scope variables: earliest or latest time
            if (isEarliest){
                $scope.userFilters.earliestTime.displayHour = userDisplayHour;
                $scope.userFilters.earliestTime.displayMinute = userDisplayMinute;
                $scope.userFilters.earliestTime.am_pm = userAmPm;
            }
            else {
                $scope.userFilters.latestTime.displayHour = userDisplayHour;
                $scope.userFilters.latestTime.displayMinute = userDisplayMinute;
                $scope.userFilters.latestTime.am_pm = userAmPm;
            }

            console.log(hour + " " + minute + " " + userDisplayHour + " " + userDisplayMinute + " " + userAmPm);
        }

        // Set actual times for comparison with json
        $scope.userFilters.earliestTime.hour = earliestHour;
        $scope.userFilters.latestTime.hour = latestHour;
        $scope.userFilters.earliestTime.minute = earliestMin;
        $scope.userFilters.latestTime.minute = latestMin;

        // Set times to display
        setDisplayTime(earliestHour, earliestMin, true);
        setDisplayTime(latestHour, latestMin, false);

        // Need to include this to update display to user
        $scope.$apply();

    });

    // Make filter column full height of page
    // $("#filterColumn").height($("#main").height());
    // console.log($("#main").height());

    var modalSuccess = new Foundation.Reveal($("#requestSuccess"), {});

}]);



/* Drive page Controller */
app.controller('driveController', ['$scope', '$http', function($scope, $http) {
    // From, To location autocomplete
    var from = document.getElementById('fromTextField');
    var to = document.getElementById('toTextField');
    var stop = document.getElementById('stopTextField');
    var autocompleteFrom = new google.maps.places.Autocomplete(from);
    var autocompleteTo = new google.maps.places.Autocomplete(to);
    var autocompleteStop = new google.maps.places.Autocomplete(stop);

    // Date picker
    $scope.count = 0;

    // Build few sample dates for demo purpose
    $scope.today = new Date();
    // $scope.date1 = new Date ($scope.today.getTime() + (7 * 24 * 3600 * 1000)); // today + 7 days
    // $scope.date2 = new Date ($scope.today.getTime() - (7 * 24 * 3600 * 1000)); // today - 7 days
    // console.log ("today=%s lastweek=%s nextweek=%s", $scope.today, $scope.date1, $scope.date2)

    // Date callback popup a message
    $scope.datePickerCB = function(pickdate, pickerid) {
        if ($scope.count++ > 1000) return; // more than 1000 mean we have a bug :)
        console.log ($scope.count, "$scope.datePickerCB pickerId=%s pickdate=%s", pickerid, pickdate.toString());
    }

    // Seats number picker
    $scope.input = {
        num: 3
    };
    $scope.getNumber = function() {
       alert('The number is: [' + $scope.input.num + ']');
    };
    $scope.onChange = function(){
       console.log('The number is Changed ', $scope.input.num);
    };

    // Cars
    $scope.cars = [
        {brand:'Toyota', model:'Camry'},
        {brand:'Audi', model:'A5'},
    ];
    $scope.addCar = function() {
        $scope.cars.push( {brand:$scope.newBrand, model:$scope.newModel} );
        $scope.carSelect = $scope.cars[$scope.cars.length-1];
    };

    // Price slider
    var elem = new Foundation.Slider($("#priceSlider"), {});

    var modal = new Foundation.Reveal($("#exampleModal1"), {});
    var modalSuccess = new Foundation.Reveal($("#postSuccess"), {});
}]);



/* Profile Controller */
app.controller('profileController', ['$scope', '$http', function($scope, $http) {

    $scope.IsVisible = true;
    // $scope.Car0IsVisible = true;
    // $scope.Car1IsVisible = true;
    $scope.IsInvisible = false;
    $scope.EditShowHide = function () {
        $scope.IsVisible = $scope.IsVisible = false;
        $scope.IsInvisible = $scope.IsInvisible = true;
    }
    $scope.UpdateShowHide = function () {
        $scope.IsVisible = $scope.IsVisible = true;
        $scope.IsInvisible = $scope.IsInvisible = false;
    }
    // $scope.Car0ModelShowHide = function () {
    //     $scope.Car0IsVisible = $scope.Car0IsVisible = false;
    // }
    // $scope.Car1ModelShowHide = function () {
    //     $scope.Car1IsVisible = $scope.Car1IsVisible = false;
    // }

    $scope.cars = [];

    $scope.addCar = function() {
        $scope.cars.push({'title':$scope.newCar, 'done': false})
        $scope.newCar = ''
    }

    $scope.deleteCar = function(index) {
        $scope.cars.splice(index, 1);
    }


    // $scope.cars = [
    //     {brand:'Toyota', model:'Camry'},
    //     {brand:'Audi', model:'A5'},
    // ];

    $scope.showAlert = function () {
        myStyle={color:'black'}
        alert("Successfully Connected!");
    }

    var modalSuccess = new Foundation.Reveal($("#connectSuccess"), {});

    $scope.loginSpotify = function(callback) {
        var CLIENT_ID = '9070716dd74248ae80bd3a8570e5ad52';
        var REDIRECT_URI = 'http://localhost:3000/#/profile/';
        function getLoginURL(scopes) {
            return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
              '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
              '&scope=' + encodeURIComponent(scopes.join(' ')) +
              '&response_type=token';
        }

        var url = getLoginURL([
            'user-read-email'
        ]);

        var width = 450,
            height = 730,
            left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);

        window.addEventListener("message", function(event) {
            var hash = JSON.parse(event.data);
            if (hash.type == 'access_token') {
                callback(hash.access_token);
            }
        }, false);

        var w = window.open(url,
                            'Spotify',
                            'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
                           );
        console.log("LOGINSPOTIFY");

    }
}]);

    // function login(callback) {
    //     var CLIENT_ID = '9070716dd74248ae80bd3a8570e5ad52';
    //     var REDIRECT_URI = 'http://localhost:3000';
    //     function getLoginURL(scopes) {
    //         return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
    //           '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
    //           '&scope=' + encodeURIComponent(scopes.join(' ')) +
    //           '&response_type=token';
    //     }

    //     var url = getLoginURL([
    //         'user-read-email'
    //     ]);

    //     var width = 450,
    //         height = 730,
    //         left = (screen.width / 2) - (width / 2),
    //         top = (screen.height / 2) - (height / 2);

    //     window.addEventListener("message", function(event) {
    //         var hash = JSON.parse(event.data);
    //         if (hash.type == 'access_token') {
    //             callback(hash.access_token);
    //         }
    //     }, false);

    //     var w = window.open(url,
    //                         'Spotify',
    //                         'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
    //                        );

    // }

    // function getUserData(accessToken) {
    //     return $.ajax({
    //         url: 'https://api.spotify.com/v1/me',
    //         headers: {
    //            'Authorization': 'Bearer ' + accessToken
    //         }
    //     });
    // }

    // var templateSource = document.getElementById('result-template').innerHTML,
    //     template = Handlebars.compile(templateSource),
    //     resultsPlaceholder = document.getElementById('result'),
    //     loginButton = document.getElementById('btn-login');

    // loginButton.addEventListener('click', function() {
    //     login(function(accessToken) {
    //         getUserData(accessToken)
    //             .then(function(response) {
    //                 loginButton.style.display = 'none';
    //                 resultsPlaceholder.innerHTML = template(response);
    //             });
    //         });
    // });





/* Notifications Controller */
app.controller('notificationsController', ['$scope', '$http', function($scope, $http) {

    // Code for notification center / messages page
    $http.get('./data/drivers.json').success(function(data){
        $scope.drivers = data;
        console.log(data);
    }).error(function(err){
        console.log(err);
    })
    $scope.addFriendText = "Add Friend";
    $scope.addFriend = function() {
        $scope.addFriendText = "Friend Request Sent";
    };

}]);




// These are just the sample ones from the base_code, don't need to edit //

/* Sample Controller */
app.controller('galleryController', ['$scope', '$http', function($scope, $http) {

    $scope.genres = [ 'Action', 'Adventure', 'Animation', 'Biography', 'Comedy', 'Crime', 'Drama', 'Family', 'Fantasy', 'Film-Noir', 'History', 'Horror', 'Music', 'Musical',
                    'Mystery', 'Romance', 'Sci-Fi', 'Sport', 'Thriller', 'War', 'Western' ];

    $scope.filters = { genre: '' };

    $http.get('./data/imdb250.json').success(function(data){

    	$scope.movies = data;
    	console.log(data);

    }).error(function(err){
    	console.log(err);
    })


}]);

// LIST view for searching
app.controller('listController', ['$scope', '$http', function($scope, $http) {

    $scope.inputText = 'Enter a search term';
    $scope.options = [{ name: 'Title', id: 'title' }, { name: 'Rank', id: 'rank' }];
    $scope.direction = [{ name: 'Ascending', id: false }, { name: 'Descending', id: true }];    //reverse?
    $scope.selectedOption = $scope.options[0];
    $scope.selectedDirection = $scope.direction[0];


    $http.get('./data/imdb250.json').success(function(data){

        $scope.movies = data;

    }).error(function(err){
        console.log(err);
    })

}]);

// DETAILS view for 1 movie at a time
app.controller('detailsController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    $http.get('./data/imdb250.json').success(function(data){
        $scope.movie = data[$routeParams.rank - 1];
        nextRank = +$routeParams.rank+1;
        prevRank = +$routeParams.rank-1;
        if(nextRank > 250)
            nextRank = 1;
        if(prevRank < 1)
            prevRank = 250;
        $scope.nextrank = nextRank;
        $scope.prevrank = prevRank;
        console.log(data);
    }).error(function(err){
        console.log(err);
    })

}]);
