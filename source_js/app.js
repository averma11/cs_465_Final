
var app = angular.module('mp3',['ngRoute', window.ngNumberPicker]);

app.config(function ($routeProvider) {

	$routeProvider
		.when('/drive/', {	// when at URL /#/drive, use partials/drive.html
			templateUrl : 'partials/drive.html',
			controller: 'driveController'
		})
		.when('/ride/', {	// when at URL /#/ride, use partials/rideresults.html
			templateUrl : 'partials/rideresults.html',
			controller: 'rideController'
		})
		.when('/profile/', {	// when at URL /#/profile, use partials/profile.html
			templateUrl : 'partials/profile.html',
			controller: 'profileController'
		})
		.when('/notifications/', {	// when at URL /#/notifications, use partials/notifications.html
			templateUrl : 'partials/notifications.html',
			controller: 'notificationsController'
		})
		.when('/test/', {	// when at URL /#/notifications, use partials/notifications.html
			templateUrl : 'partials/test.html',
			controller: 'notificationsController'
		})

		// ** These aren't used. Just here as a template //
		.when('/list/:rank', {	// angular has ways to search and sort, so dont write custom code
			templateUrl : 'partials/details.html',
			controller: 'detailsController'
		})
		.when('/list/', {	// angular has ways to search and sort, so dont write custom code
			templateUrl : 'partials/list.html',
			controller: 'listController'
		})
		.when('/gallery/', {	// angular has ways to search and sort, so dont write custom code
			templateUrl : 'partials/gallery.html',
			controller: 'galleryController'
		})
		// ^ Not used //
		// The default if you type in a URL that doesn't match any of these
		.otherwise({
			redirectTo: '/drive'
		});

})
