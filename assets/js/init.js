(function($){
  $(function(){

    $('.parallax').parallax();
    $('.modal').modal();

  }); // end of document ready
})(jQuery); // end of jQuery name space